package com.amaca.changemycolor

import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testRedButtonChangesPanelColorToRed(){
        onView(withId(R.id.red_button)).perform(ViewActions.click())
//        activityRule.scenario.
       activityRule.scenario.onActivity { activity ->
           val color = activity.findViewById<View>(R.id.color_view_panel).background as ColorDrawable
           assertEquals(color.color, ContextCompat.getColor(activity, R.color.red_1) )

       }
    }

    @Test
    fun testBlueButtonChangesPanelColorToBlue(){
        onView(withId(R.id.blue_button)).perform(ViewActions.click())
//        activityRule.scenario.
        activityRule.scenario.onActivity { activity ->
            val color = activity.findViewById<View>(R.id.color_view_panel).background as ColorDrawable
            assertEquals(color.color, ContextCompat.getColor(activity, R.color.blue_1) )

        }
    }

    @Test
    fun testGreenButtonChangesPanelColorToGreen(){
        onView(withId(R.id.green_button)).perform(ViewActions.click())
        activityRule.scenario.onActivity { activity ->
            val color = activity.findViewById<View>(R.id.color_view_panel).background as ColorDrawable
            assertEquals(color.color, ContextCompat.getColor(activity, R.color.green_1) )
        }
    }

    @Test
    fun testGreenButtonChangesPanelColorToOrange(){
        onView(withId(R.id.orange_button)).perform(ViewActions.click())
        activityRule.scenario.onActivity { activity ->
            val color = activity.findViewById<View>(R.id.color_view_panel).background as ColorDrawable
            assertEquals(color.color, ContextCompat.getColor(activity, R.color.orange_1) )
        }
    }


}

